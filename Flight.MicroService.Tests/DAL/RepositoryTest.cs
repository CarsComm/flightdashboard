﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Flight.MicroService.DAL;
using Moq;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Xunit;

namespace Flight.MicroService.Tests.DAL
{
    public class RepositoryTest
    {

        public class GetAll
        {
            private readonly IFixture _fixture;

            public GetAll()
            {
                _fixture = new Fixture().Customize(new AutoMoqCustomization());

            }
            [Fact]
            [Trait("DAL", "Repository")]
            public void Should_Return_All_Flight()
            {
                //Arrange
                var flights = _fixture.Create<List<MicroService.DAL.Entities.Flight>>();
                var mockFlightRepository = new Mock<IRepository<MicroService.DAL.Entities.Flight>>();
                mockFlightRepository.Setup(x => x.GetAll()).Returns(flights);
                
                //Act
                var response = mockFlightRepository.Object.GetAll();

                //Assert
                Assert.Equal(response.Count, flights.Count);
            }
        }
    }
}
