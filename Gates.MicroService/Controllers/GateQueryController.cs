﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using Gates.MicroService.DAL;
using Gates.MicroService.Models;
using WebApi.Infrastructure.ActionFilters;

namespace Gates.MicroService.Controllers
{
    [ModelValidationFilter]
    [CustomExceptionFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GateQueryController : ApiController
    {
        private readonly IRepository<DAL.Entities.Gate> _repository;

        public GateQueryController(IRepository<DAL.Entities.Gate> repository)
        {
            _repository = repository;
        }

        // GET: api/GateQuery
        public async Task<IHttpActionResult> Get()
        {

            var gates = _repository.GetAll();
            if (gates == null || gates.Count == 0)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<List<DAL.Entities.Gate>, List<GateModel>>(gates.ToList()));
        }

        // GET: api/GateQuery/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var gate = _repository.GetById(id);
            if (gate == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<DAL.Entities.Gate, GateModel>(gate));
        }
    }
}
