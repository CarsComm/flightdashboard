﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gates.MicroService.DAL.Entities;

namespace Gates.MicroService.DAL
{
    public class GateDummyRepository: IRepository<Entities.Gate>
    {
        public IList<Gate> GetAll()
        {
            return GateDummyDataSource.Gates;
        }

        public Gate GetById(int id)
        {
            return GateDummyDataSource.Gates.FirstOrDefault(g=>g.Id == id);
        }

        public void Add(Gate entity)
        {
            GateDummyDataSource.Gates.Add(entity);
        }

        public void Update(Gate entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Gate entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}