﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gates.MicroService.DAL.Entities
{
    public class Gate
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}