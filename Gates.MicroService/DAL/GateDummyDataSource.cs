﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Gates.MicroService.DAL.Entities;

namespace Gates.MicroService.DAL
{
    public static class GateDummyDataSource
    {
        public static List<Entities.Gate> Gates = new List<Gate>()
        {
            new Gate(){Id = 1, Number = "A1"},
            new Gate(){Id = 2, Number = "B1"}
        };
    }
}