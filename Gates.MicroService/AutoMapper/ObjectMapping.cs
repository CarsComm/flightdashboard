﻿using AutoMapper;
using Gates.MicroService.Models;

namespace Gates.MicroService.AutoMapper
{
    public static class ObjectMapping
    {
        public static void EntityToModelMap()
        {
            Mapper.CreateMap<DAL.Entities.Gate, GateModel>();
        }

        public static void ModelToEntityMap()
        {
            Mapper.CreateMap<GateModel, DAL.Entities.Gate>();
        }
    }
}