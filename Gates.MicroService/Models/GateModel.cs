﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gates.MicroService.Models
{
    public class GateModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}