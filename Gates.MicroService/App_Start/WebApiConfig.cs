﻿using System.Web.Http;
using Gates.MicroService.AutoMapper;
using Newtonsoft.Json.Serialization;

namespace Gates.MicroService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //Auto Mapper mapping
            ObjectMapping.EntityToModelMap();
            ObjectMapping.ModelToEntityMap();

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

        }
    }
}
