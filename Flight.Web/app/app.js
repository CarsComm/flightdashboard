﻿
var app = angular.module('FlightDashboardApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

app.config(function ($routeProvider) {

    $routeProvider.when("/flights", {
        controller: "flightsController",
        templateUrl: "/app/views/flights.html"
    });

    $routeProvider.when("/flight/add", {
        controller: "addflightController",
        templateUrl: "/app/views/addflight.html"
    });
    $routeProvider.when("/flights/update/:flightId", {
        controller: "updateflightController",
        templateUrl: "/app/views/updateflight.html"
    });

    $routeProvider.otherwise({ redirectTo: "/flights" });

});

var serviceBase = 'http://localhost:4132/';
var flightServiceBase = 'http://localhost:4132/';
var gateServiceBase = 'http://localhost:17398/';
app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    apiFlightServiceBaseUri: flightServiceBase,
    apiGatesServiceBaseUri: gateServiceBase,
    clientId: 'ngAuthApp'
});



