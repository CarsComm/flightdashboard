﻿'use strict';
app.controller('addflightController', ['$scope', 'flightsService', 'gatesService', function ($scope, flightsService, gatesService) {
    $scope.gates = [];
    $scope.selectedGate = {};
    var getFormattedDate = function (date) {
        var formattedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
        return formattedDate;
    };
    var addMinuteToFormattedDate = function (date, minutes) {
        //date = date + (minutes * 60 * 1000);
        var formattedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes() + minutes);
        return formattedDate;
    };
    $scope.flight =
    {
        number: '',
        arrivalTime: getFormattedDate(new Date()),
        departureTime: addMinuteToFormattedDate(new Date(), 29),
        gateId: 0
};
    //var currentDate = new Date();
    //var twentyMinutesLater = new Date(currentDate.getTime() + (20 * 60 * 1000));
    //var addMinutes = function(date, minutes) {
    //    return new Date(date.getTime() + minutes * 60000);
    //};
    gatesService.getGates().then(function (gatesResult) {
        $scope.gates = gatesResult.data;
    }, function (error) {

    });

    $scope.saveFlight = function () {
        $scope.flight.gateId = $scope.selectedGate.id;
        flightsService.addFlight($scope.flight).then(function (data) {
                var msg = data.data.message != undefined ? data.data.message : "Done";
                alert(msg);
            },
            function(error) {
                alert(error.message);
            }
            );
    };
}]);