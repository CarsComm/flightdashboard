﻿'use strict';
app.controller('flightsController', ['$scope', 'flightsService', 'gatesService', function ($scope, flightsService, gatesService) {
    $scope.selectedGate = {};
    $scope.flights = [];
    $scope.gates = [];
    flightsService.getFlights().then(function (flightsResult) {
        $scope.flights = flightsResult.data;

        gatesService.getGates().then(function (gatesResult) {
            $scope.gates = gatesResult.data;
            var flightCount = flightsResult.data.length;
            for (var i = 0; i < flightCount; i++) {
                var gate = getGate(flightsResult.data[i].gateId);
                $scope.flights[i].gateNumber = gate != null ? gate.number : flightsResult.data[i].gateId;
            }
            
        }, function (error) {

        });
    }, function (error) {
        //alert(error.data.message);
    });

    var getGate = function(id) {
        var gateLength = $scope.gates.length;
        for (var i = 0; i < gateLength; i++) {
            if ($scope.gates[i].id == id) {
                return $scope.gates[i];
            }
        }
        return null;
    };

    $scope.filterExpression = function (flight) {
        return (($scope.selectedGate == undefined||$scope.selectedGate.id == undefined) ? true : flight.gateId === $scope.selectedGate.id);
    };
}]);