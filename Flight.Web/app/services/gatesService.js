﻿'use strict';
app.factory('gatesService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiGatesServiceBaseUri;

    var gatesServiceFactory = {};

    var _getGates = function () {

        return $http.get(serviceBase + 'api/GateQuery').then(function (results) {
            return results;
        });
    };

    gatesServiceFactory.getGates = _getGates;

    return gatesServiceFactory;

}]);