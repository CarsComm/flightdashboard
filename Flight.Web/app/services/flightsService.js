﻿'use strict';
app.factory('flightsService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiFlightServiceBaseUri;

    var flightsServiceFactory = {};

    var _getFlights = function () {

        return $http.get(serviceBase + 'api/flightQuery').then(function (results) {
            return results;
        });
    };

    var _getFlight = function (flightId) {

        return $http.get(serviceBase + 'api/flightQuery?id=' + flightId).then(function (results) {
            return results;
        });
    };

    var _addFlight = function (flight) {
        return $http.post(serviceBase + "api/flightAddCommand", flight)
            .then(
                //Success
                function (result) {
                    return result;
                },
                //Error
                function (error) {
                    return error;
                }
            );

    };
    var _updateFlight = function (flight) {
        return $http.put(serviceBase + "api/FlightUpdateCommand", flight)
            .then(
                //Success
                function (result) {
                    return result;
                },
                //Error
                function (error) {
                    return error;
                }
            );

    };

    flightsServiceFactory.getFlights = _getFlights;
    flightsServiceFactory.getFlight = _getFlight;
    flightsServiceFactory.addFlight = _addFlight;
    flightsServiceFactory.updateFlight = _updateFlight;

    return flightsServiceFactory;

}]);