﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flight.MicroService.BusinessLogic
{
    public interface IValidateFlightSchedule
    {
        bool IsTimeSlotAvailable(int gateId, DateTime arrivalTime, DateTime departureTime);    
    }
}
