﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Flight.MicroService.DAL;

namespace Flight.MicroService.BusinessLogic
{
    public class ValidateFlightSchedule: IValidateFlightSchedule
    {
        private readonly IRepository<DAL.Entities.Flight> _repository;

        public ValidateFlightSchedule(IRepository<DAL.Entities.Flight> repository)
        {
            _repository = repository;
        }


        public bool IsTimeSlotAvailable(int gateId, DateTime arrivalTime, DateTime departureTime)
        {
            var flights = _repository.GetAll();
            var exist = flights.Any(f => IsBetween(f.ArrivalTime, arrivalTime, departureTime) || IsBetween(f.DepartureTime, arrivalTime, departureTime));
            return !exist;
        }

        
        private bool IsBetween(DateTime time, DateTime startTime, DateTime endTime)
        {
            if (time.TimeOfDay == startTime.TimeOfDay) return true;
            if (time.TimeOfDay == endTime.TimeOfDay) return true;

            if (startTime.TimeOfDay <= endTime.TimeOfDay)
                return (time.TimeOfDay >= startTime.TimeOfDay && time.TimeOfDay <= endTime.TimeOfDay);
            else
                return !(time.TimeOfDay >= endTime.TimeOfDay && time.TimeOfDay <= startTime.TimeOfDay);
        }
        
    }
}