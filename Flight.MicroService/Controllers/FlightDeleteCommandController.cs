﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Flight.MicroService.ActionFilters;
using Flight.MicroService.DAL;
using Flight.MicroService.Models;

namespace Flight.MicroService.Controllers
{
    [ModelValidationFilter]
    [CustomExceptionFilter]
    public class FlightDeleteCommandController : ApiController
    {
        private readonly IRepository<DAL.Entities.Flight> _repository;
        public FlightDeleteCommandController(IRepository<DAL.Entities.Flight> repository)
        {
            _repository = repository;
        }

        // DELETE: api/FlightCommand/5        
        public void Delete(int id)
        {
        }
    }
}
