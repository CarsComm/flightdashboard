﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using AutoMapper;
using Flight.MicroService.ActionFilters;
using Flight.MicroService.BusinessLogic;
using Flight.MicroService.DAL;
using Flight.MicroService.Models;

namespace Flight.MicroService.Controllers
{
    [ModelValidationFilter]
    [CustomExceptionFilter]
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    public class FlightAddCommandController : ApiController
    {
        private readonly IRepository<DAL.Entities.Flight> _repository;
        private readonly IValidateFlightSchedule _validateFlightSchedule;
        public FlightAddCommandController(IRepository<DAL.Entities.Flight> repository, IValidateFlightSchedule validateFlightSchedule)
        {
            _repository = repository;
            _validateFlightSchedule = validateFlightSchedule;
        }

        // POST: api/FlightAddCommand
        public async Task<IHttpActionResult> Post([FromBody]FlightModel model)
        {
            if (_validateFlightSchedule.IsTimeSlotAvailable(model.GateId, model.ArrivalTime, model.DepartureTime))
            {
                _repository.Add(Mapper.Map<FlightModel, DAL.Entities.Flight>(model));
                return Ok();
            }
            else
            {
                return BadRequest("Time slot is not available in the requested gate");
            }
        }

    }
}
