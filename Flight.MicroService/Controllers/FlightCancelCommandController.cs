﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Flight.MicroService.ActionFilters;
using Flight.MicroService.DAL;
using Flight.MicroService.Models;

namespace Flight.MicroService.Controllers
{
    [ModelValidationFilter]
    [CustomExceptionFilter]
    public class FlightCancelCommandController : ApiController
    {
        private readonly IRepository<DAL.Entities.Flight> _repository;
        public FlightCancelCommandController(IRepository<DAL.Entities.Flight> repository)
        {
            _repository = repository;
        }

        // PUT: api/FlightCommand/5
        //Update Arrival/Departure time
        public void Put(int id, [FromBody]string value)
        {
        }
    }
}
