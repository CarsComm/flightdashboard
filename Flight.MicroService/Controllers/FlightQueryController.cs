﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using Flight.MicroService.ActionFilters;
using Flight.MicroService.DAL;
using Flight.MicroService.Models;

namespace Flight.MicroService.Controllers
{
    [ModelValidationFilter]
    [CustomExceptionFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FlightQueryController : ApiController
    {
        private readonly IRepository<DAL.Entities.Flight> _repository;
        public FlightQueryController(IRepository<DAL.Entities.Flight> repository)
        {
            _repository = repository;
        }

        // GET: api/FlightQuery
        public async Task<IHttpActionResult> Get()
        {

            var flights = _repository.GetAll();
            if (flights == null || flights.Count == 0)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<List<DAL.Entities.Flight>, List<FlightModel>>(flights.ToList()));
        }

        // GET: api/FlightQuery/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var flight = _repository.GetById(id);
            if (flight == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<DAL.Entities.Flight, FlightModel>(flight));
        }

    }
}
