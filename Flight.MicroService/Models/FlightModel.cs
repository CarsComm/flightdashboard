﻿using System;

namespace Flight.MicroService.Models
{
    public class FlightModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime DepartureTime { get; set; }
        public int GateId { get; set; }
        public bool IsCancelled { get; set; }
    }

}