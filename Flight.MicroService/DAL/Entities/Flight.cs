﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Flight.MicroService.DAL.Entities
{
    public class Flight
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime DepartureTime { get; set; }
        public int GateId { get; set; }
        public bool IsCancelled { get; set; }
    }
}