﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Flight.MicroService.DAL
{
    public class FlightDummyRepository : IRepository<Entities.Flight>
    {
        public IList<Entities.Flight> GetAll()
        {
            return FlightDummyDataSource.Flights;
        }

        public Entities.Flight GetById(int id)
        {
           return FlightDummyDataSource.Flights.FirstOrDefault(f=>f.Id == id);
        }

        public void Add(Entities.Flight entity)
        {
            FlightDummyDataSource.Flights.Add(entity);
        }

        public void Update(Entities.Flight entity)
        {
            var flight = FlightDummyDataSource.Flights.FirstOrDefault(f => f.Id == entity.Id);
            if (flight != null)
            {
                flight.Number = entity.Number;
                flight.ArrivalTime = entity.ArrivalTime;
                flight.DepartureTime = entity.DepartureTime;
                flight.GateId = entity.GateId;
                flight.IsCancelled = entity.IsCancelled;
            }
        }

        public void Delete(Entities.Flight entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}