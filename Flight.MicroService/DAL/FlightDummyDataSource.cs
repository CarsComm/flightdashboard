﻿using System;
using System.Collections.Generic;

namespace Flight.MicroService.DAL
{
    public static class FlightDummyDataSource
    {
        public static List<Entities.Flight> Flights = new List<Entities.Flight>()
        {
            new Entities.Flight() { Id = 1, Number = "QAN001", GateId = 1, ArrivalTime = DateTime.Now, DepartureTime = DateTime.Now.AddMinutes(29)}, 
            new Entities.Flight() { Id = 2, Number = "QAN002", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30), DepartureTime = DateTime.Now.AddMinutes(59)}, 
            new Entities.Flight() { Id = 3, Number = "QAN003", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*2), DepartureTime = DateTime.Now.AddMinutes(29+(30*2))}, 
            new Entities.Flight() { Id = 4, Number = "QAN004", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*3), DepartureTime = DateTime.Now.AddMinutes(29+(30*3))}, 
            new Entities.Flight() { Id = 5, Number = "QAN005", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*4), DepartureTime = DateTime.Now.AddMinutes(29+(30*4))}, 
            new Entities.Flight() { Id = 6, Number = "QAN006", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*5), DepartureTime = DateTime.Now.AddMinutes(29+(30*5))}, 
            new Entities.Flight() { Id = 7, Number = "QAN007", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*6), DepartureTime = DateTime.Now.AddMinutes(29+(30*6))}, 
            new Entities.Flight() { Id = 8, Number = "QAN008", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*7), DepartureTime = DateTime.Now.AddMinutes(29+(30*7))}, 
            new Entities.Flight() { Id = 9, Number = "QAN009", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*8), DepartureTime = DateTime.Now.AddMinutes(29+(30*8))}, 
            new Entities.Flight() { Id = 10, Number = "QAN010", GateId = 1, ArrivalTime = DateTime.Now.AddMinutes(30*9), DepartureTime = DateTime.Now.AddMinutes(29+(30*9))}, 

            new Entities.Flight() { Id = 11, Number = "SQA001", GateId = 2, ArrivalTime = DateTime.Now, DepartureTime = DateTime.Now.AddMinutes(29)}, 
            new Entities.Flight() { Id = 12, Number = "SQA002", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30), DepartureTime = DateTime.Now.AddMinutes(59)}, 
            new Entities.Flight() { Id = 13, Number = "SQA003", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*2), DepartureTime = DateTime.Now.AddMinutes(29+(30*2))}, 
            new Entities.Flight() { Id = 14, Number = "SQA004", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*3), DepartureTime = DateTime.Now.AddMinutes(29+(30*3))}, 
            new Entities.Flight() { Id = 15, Number = "SQA005", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*4), DepartureTime = DateTime.Now.AddMinutes(29+(30*4))}, 
            new Entities.Flight() { Id = 16, Number = "SQA006", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*5), DepartureTime = DateTime.Now.AddMinutes(29+(30*5))}, 
            new Entities.Flight() { Id = 17, Number = "SQA007", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*6), DepartureTime = DateTime.Now.AddMinutes(29+(30*6))}, 
            new Entities.Flight() { Id = 18, Number = "SQA008", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*7), DepartureTime = DateTime.Now.AddMinutes(29+(30*7))}, 
            new Entities.Flight() { Id = 19, Number = "SQA009", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*8), DepartureTime = DateTime.Now.AddMinutes(29+(30*8))}, 
            new Entities.Flight() { Id = 20, Number = "SQA010", GateId = 2, ArrivalTime = DateTime.Now.AddMinutes(30*9), DepartureTime = DateTime.Now.AddMinutes(29+(30*9))}, 
        };
    }
}