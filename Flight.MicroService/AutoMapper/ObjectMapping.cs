﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Flight.MicroService.Models;

namespace Flight.MicroService.AutoMapper
{
    public static class ObjectMapping
    {
        public static void EntityToModelMap()
        {
            Mapper.CreateMap<DAL.Entities.Flight, FlightModel>();
        }

        public static void ModelToEntityMap()
        {
            Mapper.CreateMap<FlightModel, DAL.Entities.Flight>();
        }
    }
}